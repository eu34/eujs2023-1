function quiz1_2(N, M, K){
    if(N<2 || N>10){
        document.write("<h2 class='error-mess'>N parameter is not valid!!!</h2>");
        return;
    }
    if(M<2 || M>10){
        document.write("<h2 class='error-mess'>M parameter is not valid!!!</h2>");
        return;
    }
    if(K<2 || K>10){
        document.write("<h2 class='error-mess'>K parameter is not valid!!!</h2>");
        return;
    }
    document.write("<hr><hr>")
    tb = "<table class='tb'>";
        for(i=0; i<N; i++){
            tb += "<tr>";
                for(j=0; j<M; j++){
                    tb += "<td>";
                    r = Math.ceil(Math.random()*8);
                    tb += r;
                    tb += "<img class='img' src='imgs/"+r+".jpg'>";
                    tb += "</td>";
                }
            tb += "</tr>"; 
        }
    tb += "</table>";
    document.write(tb);
    document.write("<hr><hr>")
}


// document.write(quiz1_2(4, 5, 4))
quiz1_2(4, 5, 4)
