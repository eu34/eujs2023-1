var d =  new Date();
var date_info = document.querySelectorAll(".date-info");
var week_days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];


function js_date(){
    console.log(date_info);
    date_info[0].innerHTML = d;
}

function js_get_date_details(){
    console.log(d.getFullYear());
    console.log(d.getMonth()+1);
    console.log(d.getDate());
    console.log(d.getDay());
    console.log(d.getUTCHours());
    date_info[1].innerHTML = d.getFullYear()+", "+(d.getMonth()+1)+", "+d.getDate()+", "+week_days[d.getDay()]+", "+d.getUTCHours();
}

function set_time_out(){
    setTimeout(function(){
        date_info[2].innerHTML = "Hello World!!!";
        date_info[2].classList.add("text-1");
    }, 4000);
}

function set_interval(){
    setInterval(function(){
        date_info[3].innerHTML = week_days[Math.floor(Math.random()*7)];
        date_info[3].classList.add("text-1");
    }, 100);
}