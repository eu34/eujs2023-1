var inputs =  document.getElementsByTagName("input");

function isEmptyNameAndLastName(){
    if(inputs[0].value=="") inputs[0].nextElementSibling.innerText = "Name Field Is Empty!!";
    if(inputs[1].value=="")  inputs[1].nextElementSibling.innerText = "LastName Field Is Empty!!";
}

function idValidation(){
    var N = 5;
    if(inputs[2].value.length==N){
        for(let i=0; i<N; i++){
            if(isNaN(inputs[2].value[i])){
                inputs[2].nextElementSibling.innerText = "Id contains another symbols!!";
                break;
            }
        }   
    }else{
        inputs[2].nextElementSibling.innerText = "Id Lenght is incorrect!!";
    }
}

function getDateForInput(){
    var d = new Date();
    inputs[3].value = d.getDate()+"-"+d.getMonth()+"-"+d.getFullYear();
}
getDateForInput();


function checkData(){
    var labels =  document.getElementsByTagName("label");
    Array.from(labels).forEach(element => {
       element.innerText = "";
    });
    isEmptyNameAndLastName();
    idValidation(); 
}